#!/bin/sh

# This is to provide feedback if anything goes wrong to
# generate errors.

set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Run NGINX with the daemon off, so it can run in the foreground.
nginx -g 'daemon off;'
